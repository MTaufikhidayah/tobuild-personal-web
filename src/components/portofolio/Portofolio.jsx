
import React from 'react'
import "./portofolio.css"
import { ProjectsData } from "./ProjectsData.jsx"

function Portofolio() {
    return (
        <section className="portofolio container section " id="portofolio">
            <h2 className="section_title">Portofolio</h2>
            <span className="section_subtitle"> My project </span>

            <div className="portofolio_container"

            >
                {ProjectsData.map(({ id, image, title, category, demo, desc }) => {
                    return (
                        <div className="portofolio_card" key={id}>
                            <img src={image} alt="" className="portofolio_img" />
                            <div className="porfolio_card-text">
                                <h3 className="portofolio_name">{title}</h3>
                                <p className="portofolio_category">{category}</p>
                            </div>
                            <div className="portofolio_card-bottom">
                                <a href={demo} className="button button--flex">Demo
                                </a>
                                <a href={desc} className="button button-scondary">Code/Case
                                </a>
                            </div>
                        </div>
                    )
                })}
            </div>
        </section>
    )

}

export default Portofolio