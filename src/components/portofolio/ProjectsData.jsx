import newStyle from "../../assets/newStyle.png"
import ease from "../../assets/ease.png"
import traxy from "../../assets/traxy.png"
import Work4 from "../../assets/work4.jpg"

export const ProjectsData = [
  {
    id: 1,
    image: newStyle,
    title: "new style",
    category: " Web Toko online using React redux & fake API",
    demo: "https://deluxe-speculoos-3f301c.netlify.app/",
    desc: ""
  },
  {
    id: 2,
    image: ease,
    title: "Mobile App ease",
    category: "application for self-potential test and online learning",
    demo: "https://www.figma.com/proto/875H24hASreNi4zrj0JLwq/Prototype-ease?page-id=0%3A1&node-id=2%3A431&viewport=441%2C295%2C0.08&scaling=scale-down&starting-point-node-id=2%3A456",
    desc: "https://www.figma.com/proto/5ljpQXiPuYxVGagoQGo3vG/Study-Case-ease?page-id=0%3A1&node-id=2%3A28&viewport=441%2C1945%2C0.5&scaling=min-zoom"
  },
  {
    id: 3,
    image: traxy,
    title: "Mobile App Traxy",
    category: "UI/UX application GPS Monitoring System",
    demo: "https://www.figma.com/proto/sARKieynl5TJ3FCYSMf90s/TRAXY?page-id=35%3A680&node-id=35%3A681&viewport=339%2C339%2C0.25&scaling=scale-down&starting-point-node-id=35%3A681",
    desc: "https://www.figma.com/proto/sARKieynl5TJ3FCYSMf90s/TRAXY?page-id=35%3A680&node-id=55%3A1850&viewport=299%2C165%2C0.15&scaling=scale-down&starting-point-node-id=35%3A681"
  },
  {
    id: 4,
    image: Work4,
    title: "Coming soon",
    category: "---",
    demo: ""
  },
];