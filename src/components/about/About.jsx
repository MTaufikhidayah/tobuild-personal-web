import React from 'react'
import "./about.css"
import AboutImg from "../../assets/about.png"
import CV from "../../assets/Taufik-Cv.pdf"
import Info from './Info'


function About() {
    return (
        <section className="about section" id="about">
            <h2 className="section_title"> About Me </h2>
            <span className="section_subtitle"> My introduction</span>

            <div className="about_container container grid">
                <img src={AboutImg} alt="" className="about_img" />

                <div className="about_data">
                    <p className="about_subtitle">What I do</p>
                    <Info />

                    <p className="about_description">
                        UI / UX Designer I creating design for web,
                        researching, investigating and evaluating user requirements,
                        and I create web pages with React JS.
                    </p>
                    <a dowload="" href={CV} className="button button--flex">Download CV
                    </a>
                </div>
            </div>
        </section>
    )
}

export default About