import React from 'react'

function Info() {
    return (
        <div className="about_info grid">
            <div className="about_box">
                <i class="uil uil-microscope about_icon"></i>
                <h3 className="about_title">UX Research</h3>
            </div>

            <div className="about_box">
                <i class="uil uil-edit about_icon"></i>
                <h3 className="about_title">UI Design</h3>
            </div>

            <div className="about_box">
                <i class="uil uil-laptop about_icon"></i>
                <h3 className="about_title">Code and Devlop</h3>
            </div>
        </div>
    )
}

export default Info