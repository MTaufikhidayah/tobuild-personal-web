import React from 'react'

function Social() {
    return (
        <div className="home_social">
            <a href="https://linkedin.com/in/muhammad-taufik-hidayah" className="home_social-icon" target="_blank">
                <i class="uil uil-linkedin"></i>
            </a>

            <a href="https://dribbble.com/" className="home_social-icon" target="_blank">
                <i class="uil uil-medium-m"></i>
            </a>

            <a href="https://gitlab.com/MTaufikhidayah" className="home_social-icon" target="_blank">
                <i class="uil uil-gitlab"></i>
            </a>
        </div>
    )
}

export default Social