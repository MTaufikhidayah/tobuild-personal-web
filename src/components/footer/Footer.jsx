import React from 'react'
import './footer.css'

function Footer() {
    return (
        <footer className='footer'>
            <div className="container_footer">
                <h1 className="footer_title">tobuild</h1>
                <ul className="footer_list">
                    <li>
                        <a href="#about" className="footer_link">About</a>
                    </li>
                    <li>
                        <a href="#portofolio" className="footer_link">Project</a>
                    </li>
                    <li>
                        <a href="#testimonials" className="footer_link">Testimonials</a>
                    </li>
                </ul>
                <div className="footer_social">
                    <a href="https://linkedin.com/in/muhammad-taufik-hidayah" className="footer_social-icon" target="_blank">
                        <i class="uil uil-linkedin"></i>
                    </a>

                    <a href="https://dribbble.com/" className="footer_social-icon" target="_blank">
                        <i class="uil uil-medium-m"></i>
                    </a>

                    <a href="https://gitlab.com/MTaufikhidayah" className="footer_social-icon" target="_blank">
                        <i class="uil uil-gitlab"></i>
                    </a>
                </div>
                <span className='footer_copy'>&#169; 2022 by tobuild. All rigths reserved</span>
            </div>
        </footer>
    )
}

export default Footer