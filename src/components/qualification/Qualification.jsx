import React from 'react'
import { useState } from 'react'
import "./qualification.css"

const Qualification = () => {
    const [toggleState, setToggleState] = useState(1)

    const toggleTab = (index) => {
        setToggleState(index)
    }

    return (
        <section className="qualification section">
            <h2 className="section_title">Qaualification</h2>
            <span className="section_subtitle">My personal journey</span>

            <div className="qualification_container container">
                <div className="qualification_tabs">
                    <div className={toggleState === 1 ? "qualification_button qualification-active button--flex"
                        : "qualification_active button--flex"} onClick={() => toggleTab(1)}>
                        <i className="uil uil-graduation-cap qualification_icon"></i>{" "} Education
                    </div>

                    <div className={toggleState === 2 ? "qualification_button qualification-active button--flex"
                        : "qualification_active button--flex"} onClick={() => toggleTab(2)}>
                        <i className="uil uil-briefcase-alt qualification_icon"></i>{" "} Experience
                    </div>

                </div>
                <div className="qualification_sections">
                    <div className={toggleState === 1 ?
                        "qualification_content qualification_content-active" : "qualification_content"}>
                        <div className="qualification_data">
                            <div>
                                <h3 className="qualification_title">Fullstack React - Golang</h3>
                                <span className="qualification_subtitle"> SaberCode</span>
                                <div className="qualification_calendar">
                                    <i class="uil uil-calendar-alt"></i>
                                    04/2022 - Present
                                </div>
                            </div>
                            <div>
                                <span className="qualification_rounder"></span>
                                <span className="qualification_line"></span>
                            </div>
                        </div>

                        <div className="qualification_data ">
                            <div></div>
                            <div>
                                <span className="qualification_rounder"></span>
                                <span className="qualification_line"></span>
                            </div>
                            <div>
                                <h3 className="qualification_title"> Diploma 3 Enginnering Computer </h3>
                                <span className="qualification_subtitle"> Politeknik Trimaitra Karya Mandiri</span>
                                <div className="qualification_calendar">
                                    <i class="uil uil-calendar-alt"></i>
                                    2014 - 2017
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className={toggleState === 2 ?
                        "qualification_content qualification_content-active" : "qualification_content"}>
                        <div className="qualification_data">
                            <div>
                                <h3 className="qualification_title">Reserch and Devlopment Packaging</h3>
                                <span className="qualification_subtitle"> PT.Taewon Indonesia</span>
                                <div className="qualification_calendar">
                                    <i class="uil uil-calendar-alt"></i>
                                    11/2018 - 09/2021
                                </div>
                            </div>
                            <div>
                                <span className="qualification_rounder"></span>
                                <span className="qualification_line"></span>
                            </div>
                        </div>

                        <div className="qualification_data ">
                            <div></div>
                            <div>
                                <span className="qualification_rounder"></span>
                                <span className="qualification_line"></span>
                            </div>
                            <div>
                                <h3 className="qualification_title"> Drafter and Admin Project</h3>
                                <span className="qualification_subtitle"> PT.Angric Teknik Persada</span>
                                <div className="qualification_calendar">
                                    <i class="uil uil-calendar-alt"></i>
                                    2017 - 2018
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    )
}

export default Qualification